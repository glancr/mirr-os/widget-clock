# frozen_string_literal: true

module Clock
  class Configuration < WidgetInstanceConfiguration
    attribute :hour_format, :string, default: "24"
    attribute :show_seconds, :boolean,  default: true
    attribute :use_custom_timezone, :boolean, default: false
    attribute :custom_timezone, :string, default: ""

    validates :hour_format, presence: true, inclusion: { in: %w(12 24) }
    validates :custom_timezone,
              allow_blank: true,
              inclusion: { in: ActiveSupport::TimeZone.all.map { |tz| tz.tzinfo.name } }
  end
end
