# frozen_string_literal: true

module Clock
  class Engine < ::Rails::Engine
    isolate_namespace Clock
    config.generators.api_only = true

    DEFAULT_STYLES = { font_size: 400 }.freeze
  end
end
